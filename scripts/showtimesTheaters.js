$(document).ready(function () {
    function filterBySelectedOptions(selectedOptions) {
        var currentOptions = [];
    }

    function filterOptions() {
        var selectedOptions = [];
        $(".showtooltip.active[data-option]").each(function () {
            selectedOptions.push($(this).attr('data-option'));
        });

        filterBySelectedOptions(selectedOptions);

        if (selectedOptions.length == 0) {	// All selected
        } else {
            // show filter if any option selected (mobile)
            $('.showtimes_searchoptions').show();
        }
    }
    function initOptionsFilter() {

        $(".showtooltip").click(function () {

            //$(".loading").show();
            var currentOption = $(this);
            setTimeout(function () {
                if (currentOption.text() == 'All') {
                    $(".showtooltip").removeClass("active");
                    $("#lnkOptionAll").addClass("active");
                } else {
                    currentOption.toggleClass("active");
                    currentOption.blur();
                    if ($(".showtooltip.active[data-option]").length > 0)
                        $("#lnkOptionAll").removeClass("active");
                    else
                        $("#lnkOptionAll").addClass("active");
                }
                filterOptions();
                //$(".loading").hide();               
            }, 100);
        });

        // mobile switch button
        $(".showtimes_searchoptions_trigger").click(function () {
            $('.menuseparator').toggle('fast', 'linear');
            $('.showtimes_searchoptions').toggle('fast', 'linear');
            $(this).toggleClass("changearrowshowtimes");
        });

        if ($(".showtooltip.active[data-option]").length > 0) {
            $("#lnkOptionAll").removeClass("active");
            filterOptions();
        } else {
            $("#lnkOptionAll").addClass("active");
        }

    }
    initOptionsFilter();
})