var EmailRequired = false; // set email mandatory or not

$(document).ready(function () {

    $(".rateit").bind('rated', function (event, value) {
        $("#vote").val(value);
        $("#rateMsg").css('display', 'none');
    });
    $(".rateit").bind('reset', function (event, value) {
        $("#vote").val(0);
    });

    $("#ratemovie").submit(function (e) {
        $("#rateMsg,#emailMsg,#agreeMsg").hide();
        e.preventDefault();
        $("#rateMsg").hide();
        $("#rateMsg").html("");
        if (ValidateSubmission()) {
            $('.thankRating').css('display', 'block');
            $('html, body').animate({
                scrollTop: ($(".thankRating").first().offset().top - 80)
            }, 1200);
        }
    });
    // Clears any fields in the form when the user clicks on them
    $(":input").focus(function () {
        //$("#rateMsg,#emailMsg,#agreeMsg").hide();
        if ($(this).hasClass("needsfilled")) {
            $(this).val("");
            $(this).removeClass("needsfilled");
        }
    });
    $("#FreebiesOption").click(function () {
        var freebies = $("#FreebiesOption");
        if (!freebies.is(':checked')) {
            EmailRequired = false;
            $("#ReviewEmail").removeClass("needsfilled");
            $("#ReviewEmail").val('');

        } else {
            EmailRequired = true;
        }
    });

});
function ValidateEmail(email) {
    var regexEmail = new RegExp(/^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,4}$/);
    return (regexEmail.test(email));
}
function ValidateSubmission() {

    var valid = true;


    if ($("#vote").val() == 0) {
        $("#rateMsg").show();
        $("#rateMsg").html("A vote is required");
        $('html, body').animate({
            scrollTop: ($("#rateMsg").first().offset().top - 80)
        }, 1200);
        valid = false;
    }

    var email = $.trim($("#ReviewEmail").val());
    if (EmailRequired) {
        if (email == "") {
            $("#ReviewEmail").addClass("needsfilled");
            //$("#emailMsg").show();
            $("#ReviewEmail").val("Email is required");
            /*$('html, body').animate({
             scrollTop: ($("#emailMsg").first().offset().top-80)
             }, 1200);*/
            valid = false;
        }
    } else {
        if (email != "" && !ValidateEmail(email)) {
            $("#ReviewEmail").addClass("needsfilled");
            //$("#emailMsg").show();
            $("#ReviewEmail").val("Please enter a valid email address");
            /*$('html, body').animate({
             scrollTop: ($("#emailMsg").first().offset().top-80)
             }, 1200);*/
            valid = false;
        }
    }



    var termsconsent = $("#TermsConsent");
    if (!termsconsent.is(':checked')) {
        $("#agreeMsg").show();
        $("#agreeMsg").html("You must agree to the terms and conditions");
        /*
         $('html, body').animate({
         scrollTop: ($("#agreeToTermsRequired").first().offset().top-80)
         }, 1200);
         */
        valid = false;
    } else {
        $("#agreeMsg").hide();
    }
    return valid;
}