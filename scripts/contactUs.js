$(document).ready(function () {
    $('#contactForm').submit(function (e) {
        e.preventDefault();
        if (validateForm()) {
            //SUBMIT FORM
            
        }
    });

    $(":input").focus(function () {
        if ($(this).hasClass("needsfilled")) {
            $(this).val("");
            $(this).removeClass("needsfilled");
        }
    });
});

function validateForm() {
    valid = true;
    var name = $.trim($("#contactName").val());
    if (name == "") {
        $("#contactName").addClass("needsfilled");
        $("#contactName").val("This field is required");
        valid = false;
    }
    var email = $.trim($("#contactEmail").val());
    if (email == "") {
        $("#contactEmail").addClass("needsfilled");
        $("#contactEmail").val("Email is required");
        valid = false;
    }
    if (email != "" && !ValidateEmail(email)) {
        $("#contactEmail").addClass("needsfilled");
        $("#contactEmail").val("Please enter a valid email address");
        valid = false;
    }
    var comments = $.trim($("#contactComments").val());
    if (comments == "") {
        $("#contactComments").addClass("needsfilled");
        $("#contactComments").val("This field is required");
        valid = false;
    }
    /*
    var captcha = $.trim($("#contactCaptcha").val());
    if (captcha == "") {
        $("#contactCaptcha").addClass("needsfilled");
        $("#contactCaptcha").val("This field is required");
        valid = false;
    }*/
    return valid;
}
function ValidateEmail(email) {
    var regexEmail = new RegExp(/^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,4}$/);
    return (regexEmail.test(email));
}


