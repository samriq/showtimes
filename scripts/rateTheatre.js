var categories = new Array();

$(document).ready(function () {

    $(".rateit").each(function (index) {
        categories[index] = new Array();
        categories[index][0] = 0;
        categories[index][1] = $(this).attr("title");
    });

    $(".rateit").bind('rated', function (event, value) {
        categories[parseInt($(this).attr("id")) - 1][0] = value;
    });
    $(".genexp").bind('rated', function (event, value) {
        $("#rateMsg").css('display', 'none');
    });

    $("#ratetheatre").submit(function (e) {
        e.preventDefault();
        $("#rateMsg,#emailMsg,#agreeMsg").hide();
        $("#rateMsg").html("");
        if (ValidateSubmission()) {
            var submission = $("#SubmitReview");
            var email = $("#RevieweEmail");
            var str_email = $.trim(email.val());

            var title = $("#ReviewTitle");
            var str_title = $.trim(title.val());

            var comment = $("#ReviewComment");
            var str_comment = $.trim(comment.val());

            var moviemailoption = $("#MovieMailOption");
            var freebiesoption = $("#FreebiesOption");
            var termsconsent = $("#TermsConsent");
            var theaterId = $("#TheaterId");
            var is_moviemail = moviemailoption.is(':checked') ? '1' : '0';
            var is_freebies = freebiesoption.is(':checked') ? '1' : '0';

            var arryRatings = new Array();
            if (categories.length > 0) {
                for (var i = 0; i < categories.length; i++) {
                    arryRatings[i] = {CategoryId: i + 1, RatingValue: categories[i][0], CategoryDescription: categories[i][1]};
                }
            }

            var jsonStr = JSON.stringify({ParentId: theaterId.val(), Email: str_email, Title: str_title, Comment: str_comment, moviemailoption: is_moviemail, freebiesoption: is_freebies, ratings: arryRatings});

            var promisedReturn = SaveReview(jsonStr);
        }
    })

// Clears any fields in the form when the user clicks on them
    $(":input").focus(function () {
        //$("#rateMsg,#emailMsg,#agreeMsg").hide();
        if ($(this).hasClass("needsfilled")) {
            $(this).val("");
            $(this).removeClass("needsfilled");
        }
    });


});

function ValidateEmail(email) {
    var regexEmail = new RegExp(/^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,4}$/);
    return (regexEmail.test(email));
}

function ValidateSubmission() {
    var valid = true;

    if (categories[0][0] == 0) {
        $("#rateMsg").show();
        $("#rateMsg").html("General Experience is required");
        $('html, body').animate({
            scrollTop: ($("#rateTheTheatre").first().offset().top-80)
        }, 1200);

         valid = false;
    }
    var email = $.trim($("#ReviewEmail").val());
    if (email == "") {
        $("#ReviewEmail").addClass("needsfilled");
        //$("#emailMsg").show();
        $("#ReviewEmail").val("Email is required");
        /*$('html, body').animate({
            scrollTop: ($("#emailMsg").first().offset().top-80)
        }, 1200);*/
         valid = false;
    } else if (email != "" && !ValidateEmail(email)) {
        $("#ReviewEmail").addClass("needsfilled");
        //$("#emailMsg").show();
        $("#ReviewEmail").val("Please enter a valid email address");
        /*$('html, body').animate({
            scrollTop: ($("#emailMsg").first().offset().top-80)
        }, 1200);*/
        valid = false;
    }
    var freebiesoption = $("#FreebiesOption");
    var termsconsent = $("#TermsConsent");
    if (!termsconsent.is(':checked')) {
        $("#agreeMsg").show();
        $("#agreeMsg").html("You must agree to the terms and conditions");
        /*
        $('html, body').animate({
            scrollTop: ($("#agreeToTermsRequired").first().offset().top-80)
        }, 1200);*/
        valid = false;
    }else{
        $("#agreeMsg").hide();
    }
    return valid;
}
function SaveReview(jsonTxt) {
    $('.thankRating').css('display','block');
     $('html, body').animate({
            scrollTop: ($(".thankRating").first().offset().top-80)
        }, 1200);
    var varData = $.ajax({
        type: "POST",
        url: "/pages/theatre/Theatres.asmx/SaveRA",
        data: "{js:" + JSON.stringify(jsonTxt) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            OnSaveSuccess(data);
            
        },
        async: false
    });

    return varData;
}
