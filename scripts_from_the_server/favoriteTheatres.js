$(document).ready(function(){
	$(".favTheaterIcon").click(function () {
		try {
			var theatreElementId = $(this).parent().attr('id');
			var theatreId = theatreElementId.split("favTheatre_")[1];
			AddRemoveFavoriteTheater(parseInt(theatreId));
		}
		catch (Err) {
	
		}
	});
});
function loadFavoriteTheatre()
{
	if ($.cookie("favCookieList") === undefined) 
	{
		var _favCookieList = $.parseJSON($.cookie("favCookieList"));
		alert(JSON.stringify(_favCookieList));
			if (_favCookieList.length > 0) {
			var theatreId = $('.favTheaterIcon').parent().attr('id');
			if($.inArray(theatreId, _favCookieList) > -1)
			{
				$('#favTheatre_' + index + ' a').children().addClass('fa-heart').removeClass('fa-heart-o');
			}
		}
	}
}

function loadFavoriteTheatres(){
	if ($.cookie("favCookieList") === undefined) 
	{
		var _favCookieList = $.parseJSON($.cookie("favCookieList"));
		$.each(_favCookieList, function (e, index) {
			$('#favTheatre_' + index + ' a').children().addClass('fa-heart').removeClass('fa-heart-o');
		});
	}
}
function AddRemoveFavoriteTheater(theatreId) {
    if ($.cookie("favCookieList") === undefined) { //first time 
		MarkFavoriteTheater('add', theatreId);
        _favCookieList.push(theatreId);
        $.cookie("favCookieList", JSON.stringify(_favCookieList), { expires: 365, path: '/' });

    }
    else {
        _favCookieList = $.parseJSON($.cookie("favCookieList"));
        if (_favCookieList.length > 0) {
            if($.inArray(theatreId, _favCookieList) > -1){ //if id is present
				MarkFavoriteTheater('remove', theatreId);
                _favCookieList = $.grep(_favCookieList, function (x) {
                    return x != theatreId; //remove it from the array
                });
                $.cookie("favCookieList", JSON.stringify(_favCookieList), { expires: 365, path: '/' });
            }
            else {
                MarkFavoriteTheater('add', theatreId);
                _favCookieList.push(theatreId); 
                $.cookie("favCookieList", JSON.stringify(_favCookieList), { expires: 365, path: '/' });
            }
        }
        else {
            MarkFavoriteTheater('add', theatreId);
            _favCookieList.push(theatreId);
            $.cookie("favCookieList", JSON.stringify(_favCookieList), { expires: 365, path: '/' });
        }
    }
}
function MarkFavoriteTheater(action, theatreId) {
    if (action == 'add') {
        $('#favTheatre_' + theatreId + ' a').children().addClass('fa-heart').removeClass('fa-heart-o');
    }
    else if (action == 'remove') {
        $('#favTheatre_' + theatreId + ' a').children().addClass('fa-heart-o').removeClass('fa-heart');
    }
}
