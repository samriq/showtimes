var categories = new Array();
var errElemtID = "";

$(document).ready(function () {

    $(".rateit").each(function (index) {
        categories[index] = new Array();
        categories[index][0] = 0;
        categories[index][1] = $(this).attr("title");
    });

    $(".rateit").bind('rated', function (event, value) {
        categories[parseInt($(this).attr("id")) - 1][0] = value;
    });

    $("#ratetheatre").submit(function (e) {
        e.preventDefault();
        $("#rateMsg").hide();
        $("#rateMsg").html("");
        if (ValidateSubmission()) {
            var submission = $("#SubmitReview");
            var email = $("#RevieweEmail");
            var str_email = $.trim(email.val());

            var title = $("#ReviewTitle");
            var str_title = $.trim(title.val());

            var comment = $("#ReviewComment");
            var str_comment = $.trim(comment.val());

            var moviemailoption = $("#MovieMailOption");
            var freebiesoption = $("#FreebiesOption");
            var termsconsent = $("#TermsConsent");
            var theaterId = $("#TheaterId");
            var is_moviemail = moviemailoption.is(':checked') ? '1' : '0';
            var is_freebies = freebiesoption.is(':checked') ? '1' : '0';

            var arryRatings = new Array();
            if (categories.length > 0) {
                for (var i = 0; i < categories.length; i++) {
                    arryRatings[i] = {CategoryId: i + 1, RatingValue: categories[i][0], CategoryDescription: categories[i][1]};
                }
            }

            var jsonStr = JSON.stringify({ParentId: theaterId.val(), Email: str_email, Title: str_title, Comment: str_comment, moviemailoption: is_moviemail, freebiesoption: is_freebies, ratings: arryRatings});

            var promisedReturn = SaveReview(jsonStr);
        }
    })
    $.fn.scrollTo = function (target, options, callback) {
        if (typeof options == 'function' && arguments.length == 2) {
            callback = options;
            options = target;
        }
        var settings = $.extend({
            scrollTarget: target,
            offsetTop: 50,
            duration: 500,
            easing: 'swing'
        }, options);
        return this.each(function () {
            var scrollPane = $(this);
            var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
            var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
            scrollPane.animate({scrollTop: scrollY}, parseInt(settings.duration), settings.easing, function () {
                if (typeof callback == 'function') {
                    callback.call(this);
                }
            });
        });
    }


});

function ValidateEmail(email) {
    var regexEmail = new RegExp(/^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,4}$/);
    return (regexEmail.test(email));
}

function ValidateSubmission() {
    var valid = true;

    if (categories[0][0] == 0) {
        $("#rateMsg").show();
        $("#rateMsg").html("General Experience is required");
        $('body').scrollTo(300);
        return false;
    }
    var email = $.trim($("#ReviewEmail").val());
    if (email == "") {
        $("#rateMsg").show();
        $("#rateMsg").html("Email is required");
        $('body').scrollTo(300);
        return false;
    } else if (email != "" && !ValidateEmail(email)) {
        $("#rateMsg").show();
        $("#rateMsg").html("Please enter a valid email address");
        $('body').scrollTo(300);
        return false;
    }
    var freebiesoption = $("#FreebiesOption");
    var termsconsent = $("#TermsConsent");
    if (!termsconsent.is(':checked')) {
        $("#rateMsg").show();
        $("#rateMsg").html("You must agree to the terms and conditions");
        $('body').scrollTo(300);
        return false;
    }
    return valid;
}
function SaveReview(jsonTxt) {
    var varData = $.ajax({
        type: "POST",
        url: "/pages/theatre/Theatres.asmx/SaveRA",
        data: "{js:" + JSON.stringify(jsonTxt) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            OnSaveSuccess(data);
        },
        async: false
    });

    return varData;
}