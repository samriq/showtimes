
$(document).ready(function () {

    $(".rateit").bind('rated', function (event, value) {
        $("#vote").val(value);
    });
    $(".rateit").bind('reset', function (event, value) {
        $("#vote").val(0);
    });

    $("#ratemovie").submit(function (e) {
        e.preventDefault();
        $("#rateMsg").hide();
        $("#rateMsg").html("");
        if (ValidateSubmission()) {

        }
    });
    $.fn.scrollTo = function (target, options, callback) {
        if (typeof options == 'function' && arguments.length == 2) {
            callback = options;
            options = target;
        }
        var settings = $.extend({
            scrollTarget: target,
            offsetTop: 50,
            duration: 500,
            easing: 'swing'
        }, options);
        return this.each(function () {
            var scrollPane = $(this);
            var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
            var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
            scrollPane.animate({scrollTop: scrollY}, parseInt(settings.duration), settings.easing, function () {
                if (typeof callback == 'function') {
                    callback.call(this);
                }
            });
        });
    }
});
function ValidateEmail(email) {
    var regexEmail = new RegExp(/^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,4}$/);
    return (regexEmail.test(email));
}
function ValidateSubmission() {
    var valid = true;

    if ($("#vote").val() == 0) {
        $("#rateMsg").show();
        $("#rateMsg").html("A vote is required");
        $('body').scrollTo(300);
        return false;
    }
    var email = $.trim($("#ReviewEmail").val());
    if (email == "") {
        $("#rateMsg").show();
        $("#rateMsg").html("Email is required");
        $('body').scrollTo(300);
        return false;
    } else if (email != "" && !ValidateEmail(email)) {
        $("#rateMsg").show();
        $("#rateMsg").html("Please enter a valid email address");
        $('body').scrollTo(300);
        return false;
    }
    var freebiesoption = $("#FreebiesOption");
    var termsconsent = $("#TermsConsent");
    if (!termsconsent.is(':checked')) {
        $("#rateMsg").show();
        $("#rateMsg").html("You must agree to the terms and conditions");
        $('body').scrollTo(300);
        return false;
    }
    return valid;
}